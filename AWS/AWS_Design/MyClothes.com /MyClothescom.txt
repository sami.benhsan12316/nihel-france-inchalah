Démo 2 : MyClothes.com : 
   # 3 tiers = 3 Couches ... Client / Serveurs web / Serveurs BD 
   # Contraintes 3al app : Voir Contraintes MyClothes.com 
   PB 1 : Request 1 nasna3 Shopping cart -> ELB yhézha ll VM1 , Request 2 Nhb nhawl l page okhra -> ELB yhézha ll VM2 (Shopping Cart 
   t4i3 .. VM2 fibélhéch li VM1 san3ét Shopping Cart .. khatér les EC2 houma li responsable énou ykhazno él Shopping Cart) (De mm entre VM2,3) 
   RQ: Pour généraliser le problème , ce problème bch yji win maykoun el traitement mta3 Request2 lézmou des infos 3al 
   traitement mta3 request1 (Stateful application) -> DC c'est un pb de passage d'infos entres les VMs li supposés énhom 
   yétraitiw un flux ..


      SOL 1 : # Voir Capture Sol with ELB Stickiness
      ELB Stickiness (Session Affinity) .. Option tkhali ELB yforwardi el traffic li jéy mn client l VM wahda 
      .. ya3ni par exemple client b3a4 request forwardéha ELB l VM1 .. b9iyét les requests mte3o bch ykaml yfowardihomlo 
      ll VM1 .. maybadlouch el VM   
      Donwsides 1 : 
         - Fama risque énou VM1 ti7 éma njmou n7asnouha 9al .. ama pr le moment approche okhra complémentmnt (SOL2)
      
      SOL 2 : # Voir Capture Sol with Data in cookies  
      Idée : Fi 3ou4 ma tkoun el VM hia li tstocki él contenu ta3 el Shopping Cart .. ywali user howa li yéstocki contenu ta3 Shopping Cart  
      Kiféh ?: yab3a4ha fi user cookie (Concept Urgent).. blou8a okhra fkol mara yconnecti 3al ELB y9olo btw raw déjà él Shopping Cart 3andi 
      fiha x,y,z.. w haka les VMs kol ywaliw 3la 3élm bl contenu ta3 Shopping Cart .. 
      -> RQ: N9oulou énou on a assuré li app mte3na statless .. ma3néha VM ki bch tétraiti Request ma7ajéthéch bch 
      ta3rf chsar 9bal .. déjà youslélha les infos li test7a9hom lkol
      Donwsides 2: 
         - Jrayér cookies el HTTPs Requests waléw arzan .. khatér 9e3din néb34ou fi data m3aha (cookies) 
         - Risque ta3 sécu .. cookies tjm tét9sam mn hacker fl wost w ybadélk contenu el Cart donc lézmm EC2 yvalidiw cookies 
         - Taille des cookies ne doivent pas dépasser 4KB

      SOL3: # Voir Capture : Sol with session_id :  (Amélioration de SOL2 )
      Fi 3ou4 manéb34ou data kol m3a cookies .. neb34ou just session_id fl cookies .. EC2 lowl ki tjih 
      request , y9ayéd él record hétha (session_id,Cart Content) fl Elastic Cache .. fl request li mn ba3d 
      yéb3a4ha b nafs session_id w haka él EC2 él théni yjm yéjbd/ya3ml MAJ ll contenu ta3 chart ml Elastic Cache 
      , de mm (ça dépend énti w request chtalba ) pr EC2 él thélth etc .. 
      -> En gros pr ne pas compliquer .. lezm kol EC2 fi instant t ykoun 3andha session_id tjm tjbd/MAJ bih 
      el contenu ta3 Cart ml ElasticCache 
      RQ: Mch bétharoura nsajlou fi ElasticCache .. hata DynamoDB yjm yosloh .. ama puisque c' du cache .. c' plus 
      recomoodé eni nkhdm bl ElasticCache
      -> Tt les pbs de la sol 1 sont Résolus:  
         - Machékl taile: Traffic HTTP wala akhaf + no more pb de 4KB
         - Plus de sécu khatér panier mte3i msajél fl cache .. hacker mayjmch youslo 
      -> Dc bch assurina él partage d'infos entre les VMs , zéda un ElasticCache fih les infos li nhbo npartagiwhom .. 
      w hano les VMs ki thb téjbd/maj ll info témchi ll ElasticCache .. 

   PB2: Supposant nhb nsajél des données 3al user .. (Esmou,compte,@ip etc .. ) .. Kiféh na3ml ? 
   # Voir Caputre Integrating RDS DB .. Bon de mm pr les autre type de BDs .. RAS 
   -> Hna walina nahkiw 3la 3 tiers couche app 

   PB3: Traffic t9awa 3al app mte3i .. w on remarque énou aghlab users yodkhlou juste ya3ml talla 
   w yrawah (Ya3ni des requests Read 3al BD w khw).. dc charge principalement kébrét 3al BD mte3i .. 
   Kiféh netsaraf ? 
      SOL 1: # Voir Capture Scaling with Read Rep: 
      NScali el RDS b des Read Replicas .. w haka les EC2 waléw ynajmou ya9raw kima ml Master 
      kima ml les read replicas w haka charge khafét 3al BD mte3i 
      SOL2: # Voir Capture Read Rep Alternative 
      Najm nkhaféf traffic 3al RDS b éni nhawél na9ra él donnée ml cache .. kén l9atha él mékina fl 
      cache maw ok .. si nn tmchi téjbdha ml RDS w thotha fl cache bch t7a4arha ll mékinét li ba3dha 
      w yal9aw l'info fl cache  ..
      NB : Qd mm sol héthi fiha downside énou fama charge kbira 3al cache .. 

   PB4: Pour surmonter les disaster (BD/AZ ti7 bkolha) ? 
   -> MultiAZ Feature 3al Cache w RDS (Kima hkina 9bal mch ReadReplicas)
   Hot haka f mokhk .. Disaster = MultiAZ 
   
   PB5: Kiféh nconfiguri les SGs .. ? 
   SOL: 
      - SG1: ELB yé9bl traffic ml ness kol 0.0.0.0/0 
      - SG2: EC2 ye9blou traffic kén ml ELB 
      - SG3: RDS & ElasticCache ye9blou traffic kén ml EC2 
