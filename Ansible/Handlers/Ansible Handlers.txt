Q: Handlers in Ansible: Chnouma ? 
  Houma naw3 particulier ml les tasks élli mayétlansa kén 
  matlansih task okhra TA3ML un change fl serveur (ma3néha changed [ok=... ] ki tétlansa 
  ! Mahouéch kima tasks 3adyin li lancé automatically par ansible .. 
  ATTENTION: Généralement tal9ah mkhalat bl when .. ça n'a aucun rapport ! 
  -> when téthakém fl task tétlansa wla 
  -> Task kénha 3amlét change tlansi el handler si nn handler nn lansé ! 

Q: Kiféh nasna3 playbook bl handlers ? :  
    En gros dima 2 étapes: 
        - E1) Tasna3 task ta3ml change 3al serveur + notify l handler 
          # RQ: fréquament fama when .. héki tethakém el task tetlansa wla 
          ma3andha hata 3alé9a bl handler ! ATTENTION ! 
        - E2) Tasna3 handler li bch ysirlo appel automatique une fois 
        task li tnédih 3amlét change fl serveur 

Q: Exemple: 
  - name: Restart Service If Configuration File Changes
    hosts: localhost
    become: yes
    vars: 
      - ansible_become_password: user 
      - SBH: "sémi"
    tasks:
      - name: Notify Handler to Restart Service
        notify: Restart Web Service # Si task ta3ml change handler notifié .. si nn 
                                    # mch notifié 
        ansible.builtin.copy:       # el task li 3liha khdma 
          src: /home/user/SBH.py
         ! dest: /home/user/freelance-devops-infrastructure-engineer/
        when: SBH == "sémi" # Si condition correct task lansé .. si nn 
                            task nn lansée 
        !
    handlers:
        - name: Restart Web Service # Task li tétnéda jrayér task okhra 3amlt 
                                    # change fl serveur ! 
          ansible.builtin.debug:
            msg: "{{ ' sayé copina ' }}"
          
    

Q: Ekhi notify task wili field .. ? 
  field térkeb 3la task .. mahiyéch task wahadha .. déja thotha ka task wahadha 
  tjik l'erreur  héthi .. 
  PB: ERROR! no module/action detected in task. (RESOLU)
  The error appears to be in '/home/user/Ansible/ansible-playbook.yml': line 9, column 7, 
  but may be elsewhere in the file depending on the exact syntax problem.
  The offending line appears to be:
          register: logs
      - name: Test
        ^ here
      SOL : zédt task debug m3a notify ... déjà tw ça se comprend .. 
      notify matétlansa kén ma task li associé lilha ta3ml change (RESOLU)

PB: handler 9ali bch nlansih w matlanséch (matésna3 fichier li fil task ) (RESOLU)
    SOL : chouf bélkchi docker ps mafihéch log bl debug (chy) 
    SOL : badél touch b debug fl handler bch tchouf logs.. (mafamch ..)
    SOL : badél condition (zéyéd khatr déja task executé kn jét cv pas mm pas tétlansa)
    SOL : zid become etc .. (chy)
    SOL : belkchi pb fl module cmd .. jarbou fl local toul (chy..pas de pbs fih mriguél)
    25/10 : 
    SOL : ansible.cfg fih haja ? 
    SOL : A voir inchalah fl dgfip !!! bizarre !!!!!!!!! 
    SOL : ansible-playbook -vvv ansible-playbook .. l akthr logs .. majébt chy ! 
    SOL : lawéj 3la task fl DGFIP fiha handlers 
    SOL : Chouf handler fl youtube .. 
    SOL : El task li bch tnotifi el handler lézmha ta3ml modif .. !!!!!!!!!!! ( ma3néha 
    lézm tjik changed [ok = n] .. si nn handler maysirlouch appel !!!!!  )
      





