TEST: 
    SC: Playbook fih 2 tasks: 
        - T1: task python3 script.py (fih task ta3 sleep ahna nohkmou fiha 9adéh tdoum) 
        - T2: task debug 
        (ss cette forme):  
        --- 
        - name: TESTING PLAY 
        hosts: localhost 
        tasks:
            - name: TESTING TASK 
            ansible.builtin.command: 
                cmd: python3 script.py (time.sleep(n))
            async: x 
            poll: y
            - name: TESTING TASK 
            ansible.builtin.debug: 
                msg: "{{ 'test' }}"
        
    .. w on varie les tests sur sleep,async,poll (n,x,y).. pr étudier les 
    divers comportements ! 

Catégorie de TEST 1: Sans async & poll   
    Playbook 1: Sans async & poll , sleep(infini)
        # Intérprétation : Playbook w9éf fl TAST héthi .. 

Catégorie de TEST 2: Les poll > 0: 
    Playbook 1: 
        sleep: 9 
        async: 6
        poll:2
        # Intérprétation: 
            Kol 2 secondes yémchi ythabét el task wfét wla .. 
            Si au bout de 6 secondes mawfétch YGENERI ERRUEUR ! W MAYLANSICH EL TASK 
            LI MN BA3D .. ! (ATTENTION: Fl cours kétbin énou ywa9afha .. wkhw w kén méchi 
            fbéli énou yét3ada ylansi les autres tasks yétlanséw )
        # LOGS: 
            TASK [TESTING TASK] **********************************************************************
            ASYNC POLL on 127.0.0.1: jid=970628903858.65062 started=1 finished=0
            ASYNC POLL on 127.0.0.1: jid=970628903858.65062 started=1 finished=0
            ASYNC POLL on 127.0.0.1: jid=970628903858.65062 started=1 finished=0
            ASYNC FAILED on 127.0.0.1: jid=970628903858.65062
            fatal: [127.0.0.1]: FAILED! => {"async_result": {"ansible_job_id": 
            "970628903858.65062", "finished": 0, "invocation": {"module_args": 
            {"_async_dir": "/home/user/.ansible_async", "jid": "970628903858.65062", 
            "mode": "status"}}, "results_file": "/home/user/.ansible_async/970628903858
            .65062", "started": 1, "stderr": "", "stderr_lines": [], "stdout": "", 
            "stdout_lines": []}, "changed": false, "msg": "async task did not complete
            within the requested time - 6s"}

    Playbook 2:
        asyc: 6 
        poll: 2 
        processus: 4
        # Intérprétation:
        2 vérif .. l9ah kmél matstanéch 6 

    Playbook 3: 
        asyc: 6 
        poll: 2
        processus: 3
        # Intérprétation:
        2 vérif  .. l9ah kmél matstanéch 6 
        NB: 2 vérif .. mm si proc wfa ba3d 3 sec -> passage ll task li mn ba3d 
        maysir kén maythabét ansible bl poll .. ! 

Catégorie de Test 3: Les polls = 0 :
    Playbook 1: 
        async: 6 
        poll: 0
        processus: infini 
    # Intérprétation: Ytayéchha fl background w yét3ada ykaml b9iyét les tasks  

    Playbook 2:
        async: 6 
        poll: 0  
        processus: 4
    # Intérprétation: On obtien le rés du script ba3d 4 s 

    Playbook 3: 
        async: 6 
        poll: 0 
        processus: 9
    # Intérprétation: Playbook kamél lançement mte3o en laissant script.py 
    ydour fl background .. w baaaaa3d khlat el fichier li yasna3 fih script.py
    MM SI async < temps du processus 

Conclusions: 
    CC1: asyc & poll yénj7ou ll control des processus long (tadwir modél ML par exp :p ) 
    CC2: poll > 0 .. ansible mayét3ada élla maycontroli fin du processus .. si fini 9bal 
    async yét3ada .. si nn ERRUEUR (CCs plus détaillé fl Catégorie 2 )
    CC3 :  poll: 0 yhot processus fl background w yét3ada .. w howa yékml wa9t mayékml 
    valeur de async ne compte rien 

