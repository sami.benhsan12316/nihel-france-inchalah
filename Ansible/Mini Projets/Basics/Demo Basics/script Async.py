import time

# Sleep for 8 seconds
time.sleep(9)

# Message to write to a file
message = "Hello! I slept for 8 seconds."

# File path where you want to write the message
file_path = "output.txt"

# Write the message to the file
with open(file_path, "w") as file:
    file.write(message)

print(f"Message '{message}' has been written to '{file_path}'")
