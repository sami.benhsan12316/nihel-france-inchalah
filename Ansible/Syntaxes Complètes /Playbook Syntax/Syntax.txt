Cours fait avec ChatGPT:  Explain the keyword 'KEYWORD' in Ansible and provide an example that illustrates its usage
1/ Filter: (|) : héki | chnia .. ? (Ansible Filters)
    Fi kélmtin : 
    - Kima pipe fl linux (Nafs principe .. output yét3ada ta3 haja yét3ada l haja okhra 
b       ch nlansiw 3lih transformation) juste que :
    - Utilisation diffère chwaya .. Exemples:
        stat | default(false) # Taffécti valeur par default ll variable stat 
        stat | bool # Tconverti el variable stat l boolean ..
    w bien sur tnajém tenchaini barcha filtrouét wra b3a4hom
        ...... | ........ | .........

2/ default :  
    Q: 
        so default(false) is a kind of operations that i can make in filters .. and it's purpose it to give default 
        value to the variable that is passed to it ? 
    R:  
        - Hia fonction tétaplika ka filter 3la variable .. ya3tiha valeur par default en cas
        ou el variable vide , 
        non défini .. bch si c'est le cas .. playbook mayétblokéch w ykaml ykhdm 
        - Exemple 1 : M3a module debug : 
            ```yaml
            - debug:
                msg: "Variable is: {{ my_variable | default('No value provided') }}"
            # Ken variable fiha valeur .. yétficha message 3adi .. "Variable is : valeur " 
            # Ken variable mafihéch valeur ou nn défini .. yétaficha "Variable is: No value provided"
        - Exemple 2: M3a when condition : 
            ```yaml
            - name: Execute task if variable exists and is true, otherwise use default value
            debug:
                msg: "Task executed"
            when: my_variable | default(false) 
        ```
            # Ken variable défini .. w évalué à true -> Task éxécutée 
            # Ken variable nn défini .. tékhou valeur false bl default -> Task nn executé 


3/ ignore_errors: yes 
   ** Role ) 
        Keyword tet7at fard niveau m3a task , elle a pour role énou mm si task ta7et w mamchétch
        Ansible maywa9afch ykaml les autres tasks .. 
        (Nétsawar utile pr les taches li fihom doute .. ) 
   ** Exemple ) 
        --- 
        - name: TESTING PLAY 
        hosts: localhost 
        tasks: 
            - name: Debuging 
            ansible.builtin.debug:  
                msg={{ 'Début play book '}}
            - name: Tast ghalta 
            ansible.builtin.command: 
                cmd: dqsdqsdqsdq
            ignore_errors: yes  # RQ: ATTENTION : Keyword séparé kima register .. mahiyéch param 
                                # du module ... ! 
            - name: Debugging 
            ansible.builtin.debug:
                msg: "{{ ' fin play book '}}" 
            
    **) Log :   
        TASK [Tast ghalta] *************************************************************
        fatal: [127.0.0.1]: FAILED! => {"changed": false, "cmd": "dqsdqsdqsdq", "msg": "[Errno 2] No such file or directory: b'dqsdqsdqsdq'", "rc": 2, "stderr": "", "stderr_lines": [], "stdout": "", "stdout_lines": []}
        ...ignoring # ATTENTION: ywarik el ghalta fl log .. w y9olk rani ignoritha ..
        TASK [Debugging] # ATTENTION : Yakéml l'éxécution des tasks .. ! 
                        # Fl cas l3adi ykamél yékhdm 3al rou7ou w yet3ada .. ! 

4/ delegate_to (Ma3néha affecter à ... ): 
    ** Role) 
        Wa9téli 3andi task nhb nlansiha 3la serveur/group de serveur autre que éli mfaxih 
        fl hosts: .. na3ml delegate_to: serveur/group de serveurs tq ce serveur/group de serveur 
        lézmou ykoun déclaré fl Ansible inventory .. 
        RQ: mch bétharoura ce serveur/group de serveurs ykoun subset ml group li fil hosts: ... 
    ** Exemple) 
        ```yaml
        ---
        - name: Example Playbook with delegate_to
        hosts: your_target_host
        tasks:
            - name: Copy a file to a different server
            copy:
                src: /path/to/local/file.txt
                dest: /path/on/remote/server/file.txt
            delegate_to: other_server
        ``` 
    -> Dans ce playbook el copy task bch tétlansa fl other_server mch fl your_target_host
    RQ IMPORTANTE: tnajém fl delegate_to thot : 
        delegate_to: @ip 
        delegate_to: localhost
        delegate_to: server_group 
         

5/ run_once: yes :
    **) Role: 
        keyword tet7at fard niveau m3a task .. role mte3ha énha tassuri li task héki  
        bch tétlansa 3LA MEKINA WAHDA MN JEMLET GROUPE MEKINET LI THB TCONFIGURIHOM 
    **) Exemple: 
        --- 
        - name: ... 
          ansible.builtin.command:  # Task héthi tétlansa 3la mékina wahda .. mch hosts: .. kol 
            cmd: 
          run_once: yes 

    Q: Chnia mékina li bch yakhtarha Ansible?  
        Owl mékina hatétha inti fl inventory .. ma3néha kén inventory 
        [web_servers] 
        @ip1 # ylansi 3la héthi .. ! 
        @ip2
    Q: Kiféh nakhtar éna él mékina ? 
        -> delegate_to m3a run_once ... Exemple: 
        -   name: This task runs only once on host2
            debug:
                msg: "This task runs only once on host2"
            run_once: yes
            delegate_to: host2 
            # Blou8a okhra chadit 9otlou lansi task 3la mékina wahda mn jémlét el moujoudin 
            (run_once: yes) + 3la mékina héthi (delegate_to: host2 )
    Q: Real life use case ?
        - Win mahajtk b opération centralisé 3la node wéhéd : 
            Exp 1 : Thb l'opération tétlansa ken 3al control node (win nlansiw playbook) 
            tdéléguiha ll localhost 
            Exp 2 : Thb l'opération tétlansa ken 3al master node ta3 ensembe de VMs 
            Exp 3 : Thb l'opération kén 3la node central d'aggregation de logs 
            etc etc ... 


6/ Conditionals in Ansible: 
    1/ Principe : 
    -   name: Install a package only on Ubuntu
        apt:
            name: some-package
            state: present
        when: ansible_distribution == 'Ubuntu' 
    -> Si condition : 
            True : task tétlansa 
            False : yét3ada sns erreur 
    2/ Opérators : 
      equality (==), inequality (!=), greater than (>), less than (<) 
      logical operators : and && , or || , not !=
    3/ Exemples de base: 
      Exemple 1 : Condition 3la ansible fact (kima fl principe) ou cet exemple 
        - name: Install a package only on Debian-based systems
          apt:
            name: my-package
            state: present
          when: ansible_pkg_mgr == 'apt'

      Exemple 2 : Condition when 3la variable éna sana3ha bl registre 
      par exp: 
      module stat ya3tik des infos si le fichier existe wla .. w en fonction
      son existence tdécidi task lancé wla 
          .
          ```yaml
          - name: Check if a file exists and is readable
            stat:
              path: /path/to/file
            register: file_info
            when: file_info.exists and file_info.mode | int >= 420
          ```
     
      Exemple 3 : Combini les conditions : 
        You can combine multiple conditions using logical operators like `and`, 
        `or`, and `not`.

        ```yaml
        - name: Check multiple conditions
          debug:
            msg: "All conditions are met"
          when: (condition1 and condition2) or (condition3 and not condition4)
        ```
    3/ Astuce default : 

        ```yaml
        - name: Use a default value for a variable
          debug:
            msg: "Variable is not defined, using default value"
          when: my_variable | default(false) 
        ```
        # Si la variable est défini w valeur true -> Task lancée 
        # Si la variable nn défini wili fergha .. tékho valeur false (bl filtre) 
        -> Task nn lancée 
      
    4/ Astuce couplage m3a handlers : (Voir cours Handlers)

7/ Ansible loop: 
    1/ Basic Syntax: 
        ```yaml
        - # Une Task T variabilisé avec : {{ item }}"  # {{ item }} tréférenci l'objet actuel fl loop 
        with_items:           # with_items tfixi fiha les éléments li thb titéri 3lihom 
            - item1
            - item2
            - item3
        ```
        RQ: with_items: "{{ list }}" TQ list tbda variable de type list w fiha des valeurs 
        pr variabliser with_items ..
        Chbch ysir ? : T tétlansa n fois (nb d'items) .. w kol mara avec une valeur V d'item 
        
        Q: kiféh nitéri 3la un nb kbir d'items ? (Tu peux te débrouilleur de plusieurs manières 
        à voir avec ChatGPT) ama bérjoulia use principal mte3ha 3la 2,3 items au max .. haka fl 
        DGFIP .. hata bl manta9 mkch bch tlansi 100 tasks b boucle maghir maykoun 3andk 3lihom 
        un control

    2/ loop_control: 
        - Tcustumizi/tcontrolli bih el comportement ta3 el loop .. b des options
        - Fard level m3a with_items .. 
        with_items: 
            - ...
            - ...
            - ...
        loop_control:
            op: val   # ATTENTION : Pas de tiret !!!!!! 
            op: val 
            ..
            ..
            op: val 
        Option 1: 
            loop_control: 
                loop_var: SBH # fi 3ou4 référencement b {{ item }} ywali b {{ SBH }} 
        Option 2: 
            label: "Processing {{SBH}}" # El message li bch yétafficha w howa yéprocési fi un item X fixé 
        Option 3:
            pause: 5 # ya3ml pause de 5 secondes bin itération w lokhra .. Utile kil 3ada 
                    ki itér i+1 dépend ml kméla ta3 itér i 
        RQ: when: SBH != 'BEN' : Une fois une condition sarét l'itéartion héki de la loop 
        matlansihéch .. w hia manti9ia .. when 3adia pr chaque task .. 

    Full Generic Syntax : 
    --- 
    - name: TESTING PLAY 
        hosts: localhost 
        vars_files: 
        - project-vars.yml 
        tasks: 
        - name: TESTING TASK 
            ansible.builtin.module:     # Template d'un Task dépendant de {{ item }}
            params: # Using {{ item }}
            with_items: "{{ list }}" # list mdéfinia fl project-vars.yml 
            loop_control: 
            loop_var: item   # Tnajém tbadél mot clé item kén thb ..
            label: Message yjm yesta3ml {{ item }}    # Message à afficher m3a kol Task 
            pause: n        # Temps de pause bin les task 
            when: condition


8/ Lookup Plugin : 
    NB: traitina pluging hétha .. fama zéda community.general.iterable peut etre interressant 
    énk trah ..
    1/ Role:
        Plugin Nextractiw bih data mn source S (file,api,etc.. ) bch nsobouha fi variable 
        w nésta3mlouha fl playbook (fréquament utilisé m3a set_fact module  fl DGFIP ...)
    2/ Syntax (Particulièrement on démontre la syntax by example .. c' plus claire )
        Use Case 1: Read Data from a File: (OK Pour le plugin mais PB fl ansible_facts....)
        ```yaml
        - name: Textracti data ml fichier w tsobo fl fact file_contents
        set_fact:
            file_contents: "{{ lookup('file', '/path/to/file.txt') }}"
        ```
        PB: fhmétch aléh ansible_facts.file_contents ya3mli undefined .. si nn pr le plugin 
        il est ok 
        
            Use Case 2: Textracti json data ml url héka w tsobo fl Data fact: 
            - name: "....."
            ansible.builtin.set_fact: 
                Data: "{{ lookup ('url','https://jsonplaceholder.typicode.com/users')}}"

        Q: Ekhi dima utilisé m3a set_fact ? (Tawaso3 autres use cases .. )
        Q: plugin chma3néha : 
            Au sense général du terme : ye3lm rabi 
            Fl cas hétha hia fonction offerte ml Ansible (mahiyéch module séparé ) 
            testa3mlha fl playbook (Hata fl jenkins haka ta9rib)

 
    


Autres Syntaxes: 
1/ with_items (ok) , ignore_errors (ok) , regex_search , 
ansible.builtin.include_vars
loop_control: (ok)
  loop_var: (ok)
  label: (ok)
loop: ka field  (ok)... ,lookup("..","..") (ok)
pre_tasks , 
post_tasks,
delegate_facts, 
delegate_to (ok)
local_action 
task_from 
import_role 
import_task 
tags: ...
run_once: yes (ok)
serial: 1 
listen (fl handlers ...)