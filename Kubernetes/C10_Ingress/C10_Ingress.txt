**) Ingress : 
	1/ : Lwéh l'Ingress ? : (Overview) # A REFORMULER INCHALAH
		**) TECH with Nana : 
			Dans le cas ou nésta3mlou fi External service .. l'acces ll app ykoun à travers @ip_node:Port_node 
			-> bref hétha juste l cas de test .. 
			Fil prod nhébou naccédiw sur https://app.com -> Nésta3mlou Ingress 
			-> K8s cluster ywali fiha 3 composants # Voir capture avec Ingress : 
				- Ingress (Avec un pod ycontrollih li howa Ingress Controller Pod ) 
				- Internal Service 
				- Pod 
			Ingress yé9bl request ml bara .. yforwardiha ll Internal Service .. li howa bido yforwardiha ll Pod 
		**) That Devops guy : 
			1/ Entrypoint: 
				- Généralement ay bloc de MS/Serveurs .. lézmou Entrypoint yodkhol 3lih traffic HTTP ( in a form 
				of a url/protocol/hostname/dns.. ) w ba3d howa yetsaraf yforwardih lilhom .. 
				- Yjm hal Entrypoint ykoun HAProxy,LB,APIGateway,.. n'importe 
				- Fl kubernetes 3anna Ingress Controller 

			2/ Ingress Controller Role: 
				- Entrypoint point (point yodkhol 3lih traffic)
				- SSL Termination (ma3néha yjm ykhdm bl HTTPS)
				- Routing/LoadBalancing (ma3néha ydécidi néb3a4 ll MS hétha wili hétha)

			3/ Anwe3 les Ingress Controller : 
				- Ingress Controller yjm ykoun barcha anwe3 kima Nginx,HAProxy,Envoy,Traefik etc .. 
				- Kol naw3 ml les Controllers héthom bch ykoun 3ando (conf,syntaxe,details etc .. ) spécifique
				lilo .. ti asln techno wahadha lézmk déjà tbda khadm aliha .. 
				-> Hna yji el role mta3 Ingress bch ykhalik tu t'en fous mn naw3 Entrypoint .. Kiféh ? 

			4/ Ingress.yml: 
				Il suffit éni ndavlep Ingress.yml nthif .. w BE9I ON SEN FOUS TOTALEMENT !!!!!!!!!! 
				C A KUBERNETES DE TRANSFORMER LE FICHIER LL PRODUCT LI NESTA3ML FIH 

	
	2/ Multiple paths & Subdomains cases in Ingress : # Héthom li ta7fa4hom .. Des cas pratique 

		* CAS 1 ( Multiple Paths ) : 
			Prenant le cas ou site dashboard.com fih plusieurs services : analytics , 
			jeux etc nhb naccédilhom ( dashboard.com/analytics , dashboard.com/jeux etc ........ ) .. 
			(2 API Endpoints)

			apiVersion: networking.k8s.io/v1beta # Et non pas v1
			Kind: Ingress # Et non pas Service .. bch mayémchich fbélk enha service externe ! 
			metadata: 
				name: myapp-ingress
			spec: 
				rules: # Routing Rules ( règles li bch yésta3mlhom l'ingress bch yforwardi les requests )
					- host: ......com # l'adress li bch tapiha fl browser .. Voir aussi RQs host
					http: # http héthi mch hia bidha http fl http://app.com héka protocole yésta3mlo .. #Voir Q1
						paths: 
							- path:  /analytics
								backend : # backend : Chnowa SVC li fl backend bch yjéwbk 
								serviceName: analytics-service 
								servicePort: 30000 ( par exp ) 		
							- path: /shopping 
								backend: 
								serviceName: shopping-service   
								servicePort: 8080 ( par exp ) 		
							# ma3néha forwardiha l service interne de nom .. de port ... li fil service.yaml 
							li thb tforwardilo  ( IMPORTANT !!! Voir capture Ingress & His Service .. bch tchouf ingress.yml + service.yml m3a b3a4ho) 

			* RQ :  Request Flow : Request todkhol 3la http:host w tétforwarda ll service fixé fl backend 
			# Voir Q2,3 				

			Test 1 : Sob ml ras syntaxe ducode  .. (Testé)


		* CAS 2 ( Multiple Subdomains ) : 
			Prenant le cas ou nhb ykoun l'acces ss cette forme http://nom_subdomain.nomsite.com  
			# Voir capture Multiple Subdomains # Kol sous domain est représenté par un host w host yforwardi l un seul service 
			
			apiVersion: networking.k8s.io/v1beta # Et non pas v1
			Kind: Ingress # Et non pas Service 
			metadata: 
				name: myapp-ingress
			spec: 
				rules: # Routing Rules ( règles li bch yésta3mlhom l'ingress bch yforwardi les requests ) 
					- host: analytics.myapp.com # l'adress li bch tapiha fl browser .. Voir aussi RQs host
					  http: # http héthi mch hia bidha http fl http://app.com héka protocole yésta3mlo .. Question 19 : kiféh nconfiguri https ( secure connexion ) ? 
					  paths: 
							- backend: 
								serviceName: analytics-service 
								servicePort: 3000 ( par exp ) 	

					- host: shopping.myapp.com # l'adress li bch tapiha fl browser .. Voir aussi RQs host
					  http: # http héthi mch hia bidha http fl http://app.com héka protocole yésta3mlo .. Question 19 : kiféh nconfiguri https ( secure connexion ) ? 
					  paths: 
							- backend: 
								serviceName: shopping-service 
								servicePort: 30000 ( par exp ) 	

		
	3 / Ingress default backend : 
		* Contexte & Problématique : Prenant le cas ou énou Ingress jétou request mch défini 3andou ( myapp/dgfdsgd par exemple ) .. donc mch bch yal9a path yforwardi 3lih 
	request héthi .. kiféh bch yétsaraf ? 
		*  Ingress default backend : (Semha definition .. shiha)
		Howa Service spécifié ml Ingress bch yétlha b ay request  maya3rafch yétsaraf m3aha .. 
		Esmha + Port bch ykouno déjà fixé (mch énti takhtar) tal9ahom fi : 
		(kubectl describe ingress esm_ingress .. yjik paramétre ésmou default backend 
			# Voir capture default backend config .. bch tchouf config mte3 default backend w kifh 
			yothhor fl describe 
			# Voir Q4
		* Config ta3 service default-http-backend
			apiVersion: v1 
			kind: service
			metadata:
				name: default-http-backend # (Default Backend) L'esm li bch yjik fl kubectl describe ingress .. 
			spec: 
				selector: 
					app: default-response-app  # Labels ta3 pod li fih L'application li bch taffichi message
				ports: 
					- protocol: TCP 
					port: 80 # Port ta3 service tal9ah fl descriobe ingress zéda 
					targetPort: 8080  # Port ta3 app 
		-> Haka ay request mch défini fl rules ta3 ingress .. ki todkholou yforawrdiha l hal service li howa 
		bidou yforwardiha ll pod w tjih response li thbha tétafficha 
		RQ : pas besoin bch tmentionni el service hétha fl Ingress 
		# Voir Q5 

	4/ TLS Certificate : 
		* Nhébou configuriw l'Ingress bch ywali capable enou yforwardi les requests http(s) reçu ( mch http )  
		( Bch tnajém tétsaraf avec les Requests sécurisés -> Lézm nzidou un certificat TLS ) Comment le configurer ? ( Voir capture TLS Certificate ) 
		* 1er modif : fl ingress file : bch nzidou 4 stour 
			apiVersion: networking.k8s.io/v1beta # Et non pas v1
			Kind: Ingress # Et non pas Service 
			metadata: 
				name: myapp-ingress
			spec: 
				tls: # 4 linges à ajouter 
					- hosts: 
						- myapp.com 
					secret-name: ... 
				rules : # Routing Rules ( règles li bch yésta3mlhom l'ingress bch yforwardi les requests ) 
					- host : myapp.com # l'adress li bch tapiha fl browser .. Voir aussi RQs host
						http : # http héthi mch hia bidha http fl http://app.com héka protocole yésta3mlo .. Question 19 : kiféh nconfiguri https ( secure connexion ) ? 
						paths : 
							- path:  /analytics
							backend : 
								serviceName: analytics-service 
								servicePort: 30000 ( par exp ) 		
							- path: /shopping 
							backend: 
								serviceName: shopping-service  
								servicePort: 8080 ( par exp ) 		

		* 2em modif: bch nasn3ou secret ll tls certificat 
		apiVersion: v1
		kind: secret 
		metadata: 
			name: myapp-secret-tls  # Li hatou fl champs secretName fl Ingress 
			namespace: default # Fard NS m3a Ingress 
		type: kubernetes.io/tls # Type tls 
		data: 
			tls.crt: base64 encoded cert  # Ektbhom hakk w chihémk .. 
			tls.key: base64 encoded key 
	
		# Voir Q1 , Q2
		# RQ : manéktbouch https fl Ingress khatér configurina TLS .. http héki ta3 protocol mch ta3 http li tétktb 
		fl browser 