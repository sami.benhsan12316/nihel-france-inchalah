4 : deployement.yaml : (Détails)
		Kol fichier de configuration deployement.yaml fl K8s 3ando 4 parties ( Tt les fichiers configurations sont 
		sous Format YAML ) ( voir capture Contenu Yaml 1 + 2 ) 
		
		Partie 1 : Déclaration : 
			4.1 : apiVersion : kol resource 3andha apiVersion mté3ou v1 , apps/v1 , extensions/v1beta1 .. tkasarch rasék yésr m3ahom 
			haw jibhom mn hna : https://blog.knoldus.com/what-is-apiversion-in-kubernetes-part-1/#kubernetes-apiversion w hot fi mokhk li 
			hia 3ibara 3la groupe/naw3ia/.. ta3 resoursét w khw 
			4.2 : kind : Deployment , Service , Secret , ConfigMap .. ( Champs tfaxi fih el resource li 9é3éd tconfiguri fiha )
			
		Partie 2 : Métadata : 
			4.3 : metadata : tfixi fih métadata li 3al composant hétha (name , labels , annotations)
				- name : esm composant # enti ta5taro 
				- labels : # Yétfhm akthr ki tji tésta3mlou fi rabtan composant b ékhr 
					Chnouma labels ? : N'importe quel couple clé: valeur Enti ta5taro  , dima ta7t métadata , nésta3mlouh bch  : 
					- Ndécriviw l objet 
					- Ki nhb norbot composant C1 b composant C2 nésta3ml selector ta3 C1 nséléctionni bih labels ta3 C2 ( voir Demo 3 ) 
					Syntaxe : 
					metadata : 
						name : .......
						labels : 
							app: ngnix 
				- annotations : 
					1/ Chnouma ? : 
						Des metadata ss forme cle:valeur fihom des infos additionels w salém .. (la yosl7o 
						l selection .. la 3adnhom effet 3al deploiement la chy ..)
					2/ Syntaxe : 
						metadata:
							name: my-deployment
							labels:
								app: my-app
							annotations:
								key1: value1
								key2: value2
					3/ Kiféh nkharjo ? : kubectl describe deployment my-deployment + tmchi ll champs annotations 
					tal9ahom 
					4/ Use cases : 
						1 / Documenti deployment mte3k : 
						annotations:
							description: "This service provides backend API for my application."
						2/ Versioni deployment mte3k : 
						annotations:
							version: "v1.2.3"
						etc ..

		Partie 3 : Specifications : 
			4.4 : spec : tfaxi ta7tou les specifications de ce deployement ( replicas , selector , template etc ... )
				- replicas : 9adéh mn doublure ... 
					Q : Replicas ll pod ? ll deployement ? l chno béthabt ? (RESOLU)
					R : ll pod ..(Voir fl partie théorique .. bien détaillée)
				- selector : Nésta3mlouhom bch norbto composant b composant en séléctionnant des labels ( tw tchouf nchlh fi Demo rabtan kifh ) 
				- matchLabels : nzidouha ken fl selector ta3 deployement (w d'autres composants .. )..  # Voir Q3
					Q: matchLabels lézm nzid ninvesti fiha akthr .. mch wa4a7 win lézm nintégriha béthabt (RESOLU)
					R: matchLabels ma3andk win tzid tinvésti fiha bro .. séhla fil selector ta3 deployment .. w mch lézm 
					ta3rf chkoun les resources li yesta3mlouh .. hano a7fe4 template kol wahda wkhw

				
				- template : ( Voir capture Template exemple .. pour voir un exemple de template )   
					1 : Chnowa Template ? : Howa fichier configuration dédié ll pod ( nsamiwah template ) wost lfichier configuration lékbir ( Deployement ) .. 
					2 : Win nal9ah ? : ta7t spec ( specification ) .. ( attribut wosto ) 
					3 : Chnal9a fih ? : kima ay fichier configuration ( Métadata + Specifications ( version la ) ) .. specification nal9a 
					fihom containers w leurs name , image , port etc .. 
					4 : Syntaxe Template : fl capture .. A Apprendre 
				-> Dans la pratique , Généralement bch ykoun 3andna fichier deployement.yaml kbir wostou des templates associées aux pods 
				   	Q : Fama deployment fih plusieurs template ? (RESOLU) 
					R : Pour un pod ndéfiniwlo deployement .. champs replicas ya3tik 9adéh mn doublure ll pod w champs 
					template wéhéd bark yaatik el configuration ta3 el pod .. 3andk pod ékhr thb tconfigurih -> deployement 
					ekhr
					Q: les VE li nsétihom fl command , Dockerfile & deployement normalement
                	houma nafshom ? 
                    R : oui env ta3 Dockerfile ta3ml nafs li ta3mlo env ta3 Deployment.yaml .. 
                    zouz yhélou containers w ysétiw fih des VEs avec les valeurs demandés .. dc les 
                    must set VE hna houma nafshom hna .. difference tnajém tkoun fi 3alé9a m3a hajét 
                    qui conçerne déploiement bl k8s ..

				
		Partie 4 :  Status : Status Mch maktoub  fl YAML file ama Généré et Modifié automatiquement par K8s 
			En effet : K8s yé5ou Desired status ( li énti 7atito fl YAML file ) w yé5ou l  actual status 
			( li ya3rfo ml etcd ) w y9arén binéthom si les deux status differents ==> yét7arék w ya3ml modification 
			Exemple de modification : 7ajtk par exemple b 2 replicas w inti 3andk replicas bark ygénéri 2éme replicas 
			Ysamiwha self-healing feature héthi 
			Q : self-healing feature .. zid investi fiha akthr :(RESOLU)
            R : Ma3néha kubernetes capable bch yriguél rou7ou b rou7ou w yassuri li l'app high available .. w ça 
            concerne ay fonctionalité kima : 
                - pod ta7 y3awéd ylansih wahdo 
                - node ta7 ywali il évite énou yeb3a4 requests ll pod li fwostou 
                - health checks li ya3mlhom 
                etc .....
                donc plus généralement ay opération tkhalih yriguél rouho b rouho w yassuri li app fil k8s tdour
                w jawha béhy 

PB: error: error parsing deployment.yml: error converting YAML to JSON: yaml in line 29..
Sol: Thabét fi noumrou ligne héki tal9a rouhk nzélt 3la tabulation .. fasakh ligne kol 
w hot espace ..

3: Replicaset :  
	Q : Akthr tba3wil/tawaso3 fl Replicaset # Zédt chwaya fl cours .. ama zéyéd netwas3o akthr .. 
    khatr yétsna3 wa7do w yétmodifa wa7do .. ahna la nasn3ouh la nbadlouh la chy .. mahouéch teba3 pratique 
        
			3.1 : Overview : 
				Replicaset composant tétsna3 automatiquement par Deployement , moujouda fi layer
				bin Pod & Deployement .. son role yétma4él fi énha tmanagi les replicas des pods ( Voir capture Replicaset layer ) 
				en d'autre termes howa li bch ykoun responsable énou ykoun 3andk n replicas du pod 
			3.2: Command : 
				kubectl get replicaset : ta3tik replicaset + des infos alih ( Voir capture get replicaset ) 
				kubectl get replicaset <Esm_replciaset> -o yaml > replicaset.yaml : bch tkharaj yml associé ll 
				replicaset 
				RQ : Replicaset composant yétsna3 wa7do w yétmodifa wa7do .. ahna la nasn3ouh la nbadlouh la chy !
	