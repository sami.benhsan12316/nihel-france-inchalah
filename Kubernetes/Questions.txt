Theorique : 
# NB : Hna 3mélna el level théorique el matloub bch najmou nkhdmo nchalh fi environnement ta3 kubernetes .. 
ama tba9ina 3andk majél kbirrrrrrrrrrrrrrrrrrrrrr ta3 tawaso3 w details etc .. ll level ta3 CKA fi google nchalh
dc héka 3léh les réponses mahomch ma7loulin yesr ..
    PT 1 : 
        Q: Chma3néha béthabt API Server , Kubernetes API .. w srtt API Server vs Kubectl vs  Kubernetes API
        (RESOLU)
        R: 
        Kubectl : 
            (Coté client) lignét terminal  .. tlansi ménha commandét (kima ta3rf) .. les commandes héthom bch 
            yét9lbo l des requests HTTP(S) yémchiw ll API Server bch yétraitihom 
            RQ : fréquament c'est kubectl .. plus généralement Client library 
        API Server : 
            (Coté serveur) howa li mhosti fih Kubernetes API .. howa bch yé9bl requests ta3 kubectl , ytestihom , 
            yvalédhom , w yforwardihom ll API Endpoint (ml Kubernetes API ) li responsable énou tjéwb 3al request 
        Kubernetes API : 
            Hia li fiha les Fonctions/Endpoints li bch ykouno responsable énou yjéwbou les requests li tba34o 
            ml client 3an tri9 kubectl w tforwardéw ml API Server 

Request kiféh tsir : Client ylansi command kubectl (kubectl get pods par exemple) .. tet9lb l request HTTP(S) GET 
temchi ll API Server .. ytéstiha,yvalédha,yforwardiha ll API Endpoint ta3 kubernetes API li mhostia fih .. fonction 
associé ll endpoint héthi tlansi traitement mte3ha bch térja3 response HTTP affiché fl terminal ta3 kubectl du 
client 

        Q2 : Chma3néha Control Plan .. w srtt Control Plan vs Master (RESOLU)
Control Plan = API Server + Scheduler + Controller Manager + etcd  
Master Node = Node fih les composant ta3 control plan .. juste hano fl pratique nesta3mlouhom 
interchangeably
        
        Q3 : Faméch des composants physique (ou autres) ta3 master/slave njm nahki 3lihom ? .. twasa3 fl master/slave b 
        lo8a okhra (REPORTE ba3d nchalh)  
        # Pour le moment li na3rfouh largement suffisant 
n containers yétréplikéw kol (théhrli héthi shiha .. ama thbét béhy)

        Q4 : ki na3ml replicas: 2 ma3néha affectit 2 containers ll pod ? ou bien 2 replicas du pod w une fois 
        fih   (RESOLU)
replicas: n -> ma3néha bch nconfiguri n pods du pod configuré bl template li fl deployment.yml .. une fois le 
pod mconfigryin fih m containers bch ykoun 3andk n pods provisionnés avec m containers mrunyin fwost kol pod 

        Q5 : Replicas bétharoura ykoun m3a pod fard node ? (RESOLU)
Primo: 
    Décision ta3 el pod win bch ysirlo deploiement (fi ana worker node) responsable 3liha Scheduler .. li en fonction 
    les infos li 3ando 3al cluster + des critères kima (Container chtaléb resources , Kol node chfih resources dispo, 
    Pod prioritaire wla , High avail..etc # Tnajém tétwasa3 hna) yakhtar win yaffécti pod (aného worker node li bch yéthosta fih)
Secondo : 
    Ti bl3aks Scheduler a tendance énou yaffécti les pods l des nodes differents (affectation distribuée).. bch 
    en cas ou node ta7 nal9a pod yremplaci les pods li ta7o fi nodét okhra (High availabitly .. Fault Tolerance w hék 
    ri9)

        Q6 : Kube Proxy (RESOLU)
# Temporairement .. khali fmokhk énou howa 3ibara 3la service proxy .. MINSTALLI 3AL NODES KOL .. dc ye9bl w 
yforwardi les requests li jéyin ml  master fil cluster (srtt responsable énou kol service yabda bien connecté bl les pods mte3o ) 




Pratique : 
    PT 2 : 

# NB : Hna 3mélna el level de commandes el matloub bch najmou nkhdmo nchalh fi environnement ta3 kubernetes .. 
ama tba9ina 3andk majél kbirrrrrrrrrrrrrrrrrrrrrr ta3 tawaso3 w details etc .. ll level ta3 scriptouét complexe
nchalh dc héka 3léh les réponses mahomch ma7loulin yesr ..
       
        Q1 : Kiféh nstopi pod/service/node? (RESOLU)
kubectl delete pod/node/service <Esm_pod1/Esm_node1/Esm_service-name1> .. <Esm_podn/Esm_noden/Esm_service-namen>
        Q2 : Autres k8s commandes ? (khali fl level jey nchalh )

    PT 3 : 
        Q1 : Akthr tba3wil/tawaso3 fl Replicaset # Zédt chwaya fl cours .. ama zéyéd netwas3o akthr .. 
        khatr yétsna3 wa7do w yétmodifa wa7do .. ahna la nasn3ouh la nbadlouh la chy .. mahouéch teba3 pratique 
        
    PT 4 : 
        Q1 : annotations ? 
            1/ Chnouma ? : 
                Des metadata ss forme cle:valeur fihom des infos additionels w salém .. (la yosl7o 
                l selection .. la 3adnhom effet 3al deploiement la chy ..)
            2/ Syntaxe : 
                metadata:
                    name: my-deployment
                    labels:
                        app: my-app
                    annotations:
                        key1: value1
                        key2: value2
            3/ Kiféh nkharjo ? : kubectl describe deployment my-deployment + tmchi ll champs annotations 
            tal9ahom 
            4/ Use cases : 
                1 / Documenti deployment mte3k : 
                annotations:
                    description: "This service provides backend API for my application."
                2/ Versioni deployment mte3k : 
                annotations:
                    version: "v1.2.3"
                etc ..
        Q2 : Replicas ll pod ? ll deployement ? l chno béthabt ? (RESOLU)
        -> ll pod 
        Q3 : matchLabels lézm nzid ninvesti fiha akthr .. mch wa4a7 win lézm nintégriha béthabt (RESOLU)
        # matchLabels ma3andk win tzid tinvésti fiha bro .. séhla fil selector ta3 deployment .. w mch lézm 
        ta3rf chkoun les resources li yesta3mlouh .. hano a7fe4 template kol wahda wkhw

        Q4 : Fama deployment fih plusieurs template ? (RESOLU) 
            Pour un pod ndéfiniwlo deployement .. champs replicas ya3tik 9adéh mn doublure ll pod w champs 
            template wéhéd bark yaatik el configuration ta3 el pod .. 3andk pod ékhr thb tconfigurih -> deployement 
            ekhr
        Q5 : self-healing feature .. zid investi fiha akthr :(RESOLU)
            Ma3néha kubernetes capable bch yriguél rou7ou b rou7ou w yassuri li l'app high available .. w ça 
            concerne ay fonctionalité kima : 
                - pod ta7 y3awéd ylansih wahdo 
                - node ta7 ywali il évite énou yeb3a4 requests ll pod li fwostou 
                - health checks li ya3mlhom 
                etc .....
                donc plus généralement ay opération tkhalih yriguél rouho b rouho w yassuri li app fil k8s tdour
                w jawha béhy 

        Q6 : héka fichier status kiféh jébéto ? (NON RESOLU) .. jéwbni 3la status ekhr .. bref mch importante 
        el question 

    PT 5 : 
        Q1 : mch wathha yesr hal Remarque ! .. faut se plonger fihal hkéya .. wraha baaaaaaarcha khrom .. kifh 
        NodePort yét7al fl nodes (RESOLU)
        Q2 : Kiféh tdour request ? port externe - port interne - port container .. 3léh port intern lwéh yoslo7 
        .. 3léh mch port extern - container toul ? (RESOLU)
        Q3 : Les replicas kol ydouro fi nafs el port ? (RESOLU)
        Q4 : les VE li nsétihom fl command , Dockerfile & deployement normalement
                houma nafshom ? 
                    Réponse : oui env ta3 Dockerfile ta3ml nafs li ta3mlo env ta3 Deployment.yaml .. 
                    zouz yhélou containers w ysétiw fih des VEs avec les valeurs demandés .. dc les 
                    must set VE hna houma nafshom hna .. difference tnajém tkoun fi 3alé9a m3a hajét 
                    qui conçerne déploiement bl k8s ..

        Q5 : service accesible 3al @ip li ya3tihélo minikube (bl commande (RESOLU)
        minikube service ... ) ou 3ali fl tableau get svc .. khatr les 2 sont differents 
        wahda bl 10. .. w lokhra nl 192. ... Aslan pk 3ando 2 @ip ? 
        # Hal pb yjik ken minikube cluster .. fi cluster 3al cloud .. 

        Q6 : PB ouvert : 
        # Test 1 : 
            - Esna3 pgadmin container b 1 replicas (Deployement.yaml) + ejbed  
            mp & user ml secret.yaml 
            - NodePort Service accessible ml bara w yforwardi ll pgadmin 

            PB OUVERT : 
            l'@ip + port li jébnéhom ml tableau/minikube service .. svc maykhaliwnich accesible 
                    "/snap/core20/current/lib/x86_64-linux-gnu/libstdc++.so.6: version `GLIBCXX_3.4.29' not found (required by /lib/x86_64-linux-gnu/libproxy.so.1)
                Failed to load module: /home/user/snap/code/common/.cache/gio-modules/libgiolibproxy.so
                Opening in existing browser session.
                libva error: vaGetDriverNameByIndex() failed with unknown libva error, 
                driver_name = (null)" 
                                    
                sol 1 : minikube describe service esm service (Chy .. shih taffécta ll 
                service @ip éma mch accesible 3liha encore)
                sol 2 : update minikube version (eventuellement machékl ta3 compatibilité) (Chy)
                sol 3 : updati GLIBCXX , libproxy  (Chy )

            PB : 3mélt update ll port li ydour 3lih pod fl kubecluster .. w be9i port matbadélch 
            -> # Question : kubectl rollout status deployment pg-admin-depl w matéb3o 
            (en gros k8s yupdati wahda wahda cluster bch maytihéch pod .. héka aléh modif 
            maysirch sur place ama lézm nzid nfhmha nchalh )
        Q7 : L'idée ta3 service LoadBalancer hétha est un peu ambigue .. (RESOLU)
        Q8 : Bizarre 3léh nodePort ma3andouch externalIP -> Q8  (RESOLU)
        Q9 : : Supposant li 3andi 2 pods nhb nforwardilhom .. w kol pod fl deployment mte3o el service kiféh 
        lézmou yséléctionni ? les 2 labels ta3 2 deployments ? .. ? 
        Q10 : headless service bkolo hors sujet .. 
        Q11 : Kiféh na3rf kol pod affecté à quel node : (RESOLU)
        kubectl get pod <esm_pod> -o wide .. yjik champs node li ydour fih pod


    PT 6 : 
        Q1 : matchLabels : faut s'investiguer de plus (RESOLU)
        Q2 : service yetsama selectionna label ta3 deployement wili ta3 les pods  (RESOLU)
        Q3 : Lezm nfhmo akthr chma3néha composant yséléctionni les label ta3 composant ekhr (RESOLU)		
        Q4 : service yséléctionni labels des pods ou labels des deployments (labels pod 3la mafhmt fi projet K8s MS  .. label 
        deployment no idea chno ya3mlou bih ) (RESOLU)
    
    PT 8 :
        Q1 : Chma3néha Controle Plan .. ? (RESOLU)
        Q2 : Ekhi minikube 3ando des logs ? (oui) minikube logs .. (REPORTE)
        # Fama min 10 blayés tnajém tal9a fihom les logs fl k8s .. sujet à aborder inchalah fl admin 
        sys 
        Q3 : chma3néha kubernetes events ?  (REPORTE)
            * Les events sont générés ml (API server, controller manager, scheduler, kubelet,kubectl commands )  ka réponse 3la ay 
            changement ysir fl cluster .. yésl7ou ll débuggage du cluster 
            * Ml event najm na3rf el ressource él concerné b hal event 
            * Zouz anwé3 : Normal & Warning events 
            * Kol event 3ando : 
            ressource li concerné biha , raison : For example, "Scheduled," "Created," "FailedScheduling," and "OutOfMemory" are common event reasons,
            Timestamps(wa9th sar) ,message(akthr details),sévère ou pas 
            * Kiféh njibom : 
                ```bash
                kubectl get events
                kubectl describe pod <pod-name>
                ```
            # Kélmtin khféf .. éma la3b shih bihom fl AdminSys 
       
        Q4 : Détaille akthr ce truc : 3la star ta3 containers VSCode cha3aml dénya b warning hétha : (RESOLU)
        " One or more containers do not have resource limits - this could starve other processes "
            Solution : 
                resources:
                    limits:
                        cpu: "1"
                        memory: "1Gi"
        R: resources , request , limit : # Tfaxi 9adéh el pod taléb resource (cpu w mémoire ram) w 9adéh tolérance de dépassement 
                resources: 
                   request: 
                        cpu: 100m # milicore  # RQ : nwémér héthi adéquate m3a demande normal .. l pod ma3andouch haja spécial t5alih yotlob yésr  
                        memory: 64Mi # mebibyte 
                   limit: # Pod ynajém yhéb yéstahlék akthr mli énti 7atétlou fl request .. hna ysir un pb énou .. enou pod ywali ydour yconsommi les resources 
                                                                                  résérvé ll podét lokhra .. dc wa9tha lézm nlimitiw cpu w memoire li ynajém un pod yé5ouha 
                        cpu: 200m 
                        memory: 128Mi  

                RQ : ATTENTION : rod bélk ml les valeurs li t7othom .. une fois t7ot des valeurs akbar ml capacité ta3 akbar Node fl Cluster .. el pod jamais 
                ysirou sheduling 
                RQ : hanou 7othom fl config des pods kol .. w ajusti les valeurs selon MS chnowa yjm yékl (info ml dev team héthi .. ) par exemple redis 
                lézmo memoire akthr w cpu a9al .. 
        
        Q5 : How to stop a running pod ? zid thabét kifh ?  (NON RESOLU)
            # Kubectl delete pod <pod_name> 
            PB : Pod y3awéd yrestarti wa7do .. 
        Q6 : Chnowa far9 bin label el deployement.yaml w label el template ? (SEMI RESOLU)
            # Label el template howa ta3 el pod w howa li norbto bih el service w deployment 
            # Label el deployment.yml (No idea chya3mlou bih)
		Q7: ekhi service yetrbat b label el deployement wili label el tempalte ? (RESOLU) 
            El template .. ma3ando hata 3alé9a b label el deployment 
        Q8 : Chnowa el service li moujoud déja ml lowl fl kubectl get pod ? fch ya3ml ? (NON RESOLU)
        Q9 : Il faut apprendre éni nkhdm bl doc .. (mm si chatgpt peut faire l'affaire) (REPORTE)
        Q10: Chnowa hal configMap li moujoud déjà .. kube-root-ca.crt   1      101m (NON RESOLU)
        Q11 : CrashLoopBackOff .. Lezm nzid nfhmha nchalah .. ka concept + résolution du pb (SEMI RESOLU + REPORTE ll admin Sys)
		# PB : CrashLoopBackOff fl status ( Non Résolu)
			sol1 : logs esm_pod .. 
				PB : jeb li fama error de connexion .. express manajmch yconnecti 3al mongo ..
					sol 1.1 : syntaxe (chy) 
        CrashLoopBackOff: Status y9olk éli pod taya7 w Scheduler 9é3éd yrestarti fih w bé9i chy 
        Q12 : Tawaso3 Fl Pratique : Exemple ta3 utilisation okhra ll Minikube cluster .. 
		postgres w pg admin par exp (Fait fl section services )
		Q13 :  Tawaso3 fl Théorique : en gros enou na3mlo projection ll théorique li fou9 3al cluster hétha (REPORTE)
		+ hajaét okhra .. (kublet,kubeproxy , etc .. winhom fl minikube) ... b7ar hétha ..

    PT 9 : 
        Q1 : ba3wl fi wost kol namespace chouf chfi wosto ... hell les pods , les composants en général .. (REPORTE)
        Q2 : Kiféh nasna3 un composant dans un NS Particulier ? (RESOLU)
            M1 : Tzid hétha 
                namespace: Esm_namespace # fl metadata 
                TEST : Jarab si le NS n'existe pas fl lowl 
            M2 : 
                - kubens NameSpace 
                - Esna3 yml mte3k 
                - kubectl apply -f .. 
                -> Composant yétsna3 ss héka NS 
    PT 10 : 
        Q1 : kiféh nconfiguri https ( secure connexion ) ?  (TLS Certif .. Voir le pt)
        Q2 : RQ :  host li fil config ta3 Ingress lézmo yétrbat b @ip ta3 Node ykoun howa Entry point ( howa li todkhol 3lih Request ) 
			# Question : chma3néha .. déja rbatna fl backénd ( Node hétha yjm ykoun wost wili bara K8s Cluster ) 
			-> Voir E3 fi ekhr démo dans ce pt 
        Q3 : Paths ( féhmina intuitivement éma zid wa4a7ha ) vs route !? 
        Q4 : Kiféh nésn3 default-backend ? win pod associé (default-response-app) ? 3léh targetPort 8080 ? TT est résolu 
        - E1 : Nansa3 pod fih affichage du message de response (Tal9ah container san3ou developpeur)
        - E2 : Nasna3 Internal service haka : # Voir capture default backend config 
        Q5 : Kifeh ingress bch ya3rf li howa lezm yforwardi el requests el taychin l hal service ? 
        Q6 : Les 4 ligne & les détails du secret à comprendre nchalah ..
	    Q7 : kiféh nconfiguri TLS Certif fl multiple Subdomains ? 
        Q8 : Request 3la chkoun todkhol béthabt Ingress controller pod wili Ingress bido
        Q9 : PB : Mafaméch aslan Kubernetes Dashboard (NON RESOLU .. W9EFNA HNA DEJA ... )
			sol 1 : Install bl chatgpt (Chy .. les installations données kol outdated)
			sol 2 : sudo sysctl net/netfilter/nf_conntrack_max=131072  (Chy ) 
				minikube stop
				<close previously opened dashboard process> - for me ctrl+c in separate terminal, but maybe you'd have to kill it
				minikube start
				minikube dashboard
        Q10 : kube-system ? -> -n ma3néha namespace .. pod téba3 system
        Q11 : billéhi fothha la7kéya w efhm chma3néha ngnix ?  
        Q12 : /etc/hosts zid éfhmou akthr 


# Autres: 
1/ kubectl delete pod <esm_pod> .. raj3et pod ml crash l running 
2/ Headless service pt mohém barcha fl tawaso3 
3/ Bitnami 
4/ Volume associé au pod wili au container ?
    
Q: Kiféh nstopi pod/service/node san3ou bl deployment.yml? (NON RESOLU)
		R: kubectl delete pod/node/service <Esm_pod1/Esm_node1/Esm_service-name1> .. <Esm_podn/Esm_noden/Esm_service-namen>
        PB : Haka dima yrestartiw wahdo + chouf el set-replicas=0 + chouf él tafsikh des pods crées 
		avec helm packg 



Reflex : 
PB 1 : error: resource mapping not found for name: "mongo-deployement" namespace: "" from "mongo.yaml": no matches for kind "Deployement" in version "v1" ensure CRDs are installed first ( Error ghalta fi version wili kind ) 
	solutions intuitives : 
	sol 1 : badél b apps/v1 
	sol 2 : 3andi ghalta fl deployment klma kétbha bl 8alét ( Résolu ) 
	RQ : ama zéda il faut mettre l'accent 3al version li nhotha fl apiversion w kiféh ntala3ha etc .. 
	solutions net : 
	sol 1 : minikube start --logtostderr --v=0 --bootstrapper=localkube --vm-driver virtualbox  ( chy ) 
	
PB 2 : Error from server (BadRequest): error when creating "mongo.yaml": Deployment in version "v1" cannot be handled as a Deployment: json: cannot unmarshal string into Go struct field LabelSelector.spec.selector.matchLabels of type map[string]string
	Solutions Intuitive : 
	sol 1 : ként 3andi app:mongodb ( lés9a ) raditha app: mongodb
	# RQ : fama zéda 
	 Go struct field Container.spec.template.spec.containers.ports of type v1.ContainerPort
	solution taba3 win y9olk go w sala7 ghaltét syntaxe li 3andk 
	
PB 3: The Service "mongo_service" is invalid: metadata.name: Invalid value: "mongo_service": a DNS-1035 label must consist of lower case alphanumeric characters or '-', start with an alphabetic character, and end with an alphanumeric character (e.g. 'my-name',  or 'abc-123', regex used for validation is '[a-z]([-a-z0-9]*[a-z0-9])?') : 
	solutions intuitives : 
		sol1 : badélna _ b - ( Résolu ) 
		
PB 4: MESA-INTEL: warning: Performance support disabled, consider sysctl dev.i915.perf_stream_paranoid=0 ( Blockage ) 
libva error: vaGetDriverNameByIndex() failed with unknown libva error, driver_name = (null)
	solutions intuitives : 
	sol1 : 
	solutions net :
	sol 2 : sudo crontab -e  + ... ( chy ) 
	sol 3 : https://ubuntuforums.org/showthread.php?t=2457426 ( tna7at loula 9a3dt libva error )



	


Questions : 
	* Tarki7 environnement lkol Techno ( Plugins etc .. ) 
	* Majuscule yhém fl k8s ? en générale les autres règles fl yaml files ?
	* Replicas du pod vs Replicas du Deployement ?
	* Kiféh nakhtar les var d'env ? ( Houma raw yétjébdo ml docker hub .. image offciel ) 
	* minikube lézm ninitialisih kol man7ell terminal jdid ? 
	* apiVersion héki aslan chnia ? 3al version li nhotha fl apiversion w kiféh ntala3ha etc .. ?
	* Que fait héthi ?  kubectl api-resources | grep ...
	* chma3néha replicaset ?
	* PB wa4a7 fl lecture des docs en général ( particulièrement choix des variables d'env )
Tba3wil : 

Remarques : 

RQ : 
	RQ 1 : fi syntaxe YAML wa9t attribut bch tsatratlou barcha hajét téb3éto ta3ml # Question : el - bl indentation wla ?
	attribut :
	- 
	-
	-
	etc ..  
	Exemple : 
	containers :
	- 
	
	-
	
	-
	ou 
	ports :
	- containerPort : 27017 
	etc .. 
	RQ 2 : Nsirou néktbo haka : champs: valeur 
	RQ 3 : Mafama 7ata ména3 bch na3mlou nafs label bin deployement w les pods li ydourou fih

Kiféh nasna3 resource fi NS .. kubens NS w apply fl NS maykafich