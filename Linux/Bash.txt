Cours 1: Bash Script Basics : 
	NB 1 : 
		AY command kont tlanci fiha 3al terminal tnajém zéda tw tlansiha fl script bash mté3k .. w bch 
		ta3ml nafs li ta3lou héki command fi terminal 3adi ( output , san3an dossier .. n'importe )  
		ls $Dossier , mkdir $Dossier etc ......-> Fl khdma 3andk les commandes ta3 AWS Cli etc .. fi wost script 
		ytaya7 infra par exemple .. 
		TEST: Script s8ir fih des commandes ls,pwd etc .. (OK)
		CC: Script bash flkher maw ella suite de commandes + for,if,etc ... 

	NB 2 : Editeur vi 
	# NB: Lézm nzid nétwasa3 akthr fi qq shorcut okhrin .. pr le momnt cv .. 
		1. vi /path_to_file # yhell fichier bch téditih
		2. boutton a # bch twali tjm tbda tktb fih 
		3. Echape + un nb  + d  + entré # yfasakh nb lignes à partir ml postion li inti fiha 
		4. Echape + d + d + entré # yfasakh ligne li inti fiha 
		5. Echape + :wq + entré tokhroj 

	1. SheBang Line : (Mlkher lézm dima ta3mlha fl lowl bch yétsama bash script)
		#!/bin/bash      -> Hétha yétsama shebang line , ykhalik ta5tar script .sh mté3k bch yétlansa 3la sh wili bash 
		Q: Mieux comprendre .. kiféh na3ml un executable python avec la ligne du shebang .. 
		
	2. Variables :
		2.1 : Syntaxe : Esm_variable=Valeur Exemple : file_name=config.yaml  ( Nafs l mafhoum ta3 variable fil programmation 3adia ) 
		RQ : ATTENTION mafaméch espace fl = si nn erreur ( Variable nafs def kima fi ay programmation 3adia ) si nn bch yintérpréti esm 
		el variable ka commande w ykharajlk hal erreur: 
		./Esm_script.sh: ligne x: Esm variable : commande introuvable  
		2.2 : $ sign ?  a pour but d'extraire le contenu d'une variable/commande 
			Exemple 1 : echo ".... $Esm_variable ...... " 
			Exemple 2 : ATTENTION : Dima on compare le contenu de la variable b haja okhra .. Mch variable b haja okhra 
				exp : if [ $Name == "SAMI" ] mch if [ Name == Sami ] 
			todkholch ba3thk command éktbha kima exactement fi ay terminal 3adi ( ls Test ) pas besoin de ( ls "Test" ) 
			RQ : ki tébda command générét erreur .. yjib erreur 3al script bkolo 
			Exemple 3 : variable=$(command) bch nsavi l'output ta3 command héthi fil variable 
						RQ : command tétlansa fl dossier li moujoud fih el script 
			Exemple 4 : Les expression artihmétiques :
			Ki nhb naffécti l variable une expression artihmétique na3ml haka 
			VARIABLEY=$(($VARIABLEX +/-/* 1))
			# VARIABLEY ma8ir $  .. modif ysir fl variable mch fl valeur de la variable
			# ((...)) nhotou wostha une expression artihmétique 
			# $((...)) nextractiw biha valeur ta3 expression arithmétique 
			# $VARIABLEX .. les +/-/* etc .. ysiro bin des valeurs 
		Q: Les opérateurs ... ? -eq -ne == ??? 
		R: Hano fl if makhdmouin mlih .. 
		
	3. Passer les arguments ml terminal : 
		3.1: Méthode 1 : Dns le cas ou ta3rf user 9adéh bch yda5al mn variables & thb les variables 3andhom asémi : 
			# Wost script : read -p " Message " Variable1 Variable2 ..... Variablen 
			# Réferencement fl script : echo " .... $Variable1 $Variable2 .....  $Variablen " 
			# Ml terminal : Ki tdakhal les variables valeur_variable_1 valeur_variable_2 .. # lézm espace bin les valeurs + kol fard star matarja3ch 
			ll star bin tad5ilét valeur w valeur okhra 
			RQ : Kén nombre de valeurs li da5altou > nombre de variables fl script variable lokhrania bch yétsab fih li da5alto lkol
		3.2 : Méthode 2 : 
			# Wost script : Réferencement $1 , $2 , ..... $9 ( ma3néha héka le 1ér argument li inti bch t3adih ml terminal , 2éme argument .... etc ) 
			w mafaméch ligne totlob fiha l'user bch yda5al valeurat 
			# Ml terminal : /path_to_script val1 val2 ... valn 
			val1 -> $1 , val2 -> $2 ...... valn -> $n 
			Remarques VALIDES QUE POUR CETTE METHODE : 
				RQ 1 : $* : ta3tik tt les paramètres li 3adithom lkol .. utile khatrou itérable .. 
					for var in $* 
						do 	
							# Opé sur var 
						done  
				RQ 2 : $# : ta3tik 9adéh da5alt mn paramètres
		# Voir Q1 

	4. if Statement : 
		4.1 : Syntaxe : 
			SBH=19
			if [ $SBH == 9 ]
			then
					echo "non"
			elif [ $SBH == 19 ]
			then
					echo "oui"
			else # Pas de then avec else si nn erreur 
					echo "K"
			fi
			# NB : ATTENTION : L'espace obligatoire ba3d [ w 9bal ] w HATA fi wost condition 
			(bin $SBH w == w 9 ) -> Jomla 4 espaces sont obligatoires
			PB: line 3: [: missing `]' (Résolu)
			-> Tt simplement fasakh condition li ktébtha bin [] w 3awéd éktbha  
			Espace ba3d [ w 9bal ] w espace wost condition .. les espaces délicats yésr .. 3guél 
			.. Des espaces invisibles ya3mloulk fi mochkla !! So ATTENTION ! .. 

		4.2 : Que écrire fl Test Statements ?: (TAWASO3 OBLIGATOIRE HNA)
			4.2.1 : Test de Fichiers/Dossiers :
				-> -d ou -f  .. 
					-> Exp 1 : if [ -f Test.sh ] 
					-> Exp 2 : if [ -d SBH ] 
				RQ : Plus généralement hot -f/-d FULL_PATH_FILE/FOLDER
			4.2.2 : Test 3al les strings : 
			# String 3andk choix kima thb " " ou ' ' 
				Egalité : = ou == zouz kifkif [ $Variable = ou == "valeur" ]
				Different : != 
				Taille : 
					-z $Variable # kénha chaine fér8a -> True , False si nn  ( z : zero ) 
					-n $Variale #  kénha chaine fér8a -> False , True si nn  ( n : non-zero ) 
				str # Question 
			4.2.3: Test sur les integers : (-eq,-lt,-gt etc .. )
			Test : Hano tasti les conditions .. kima thb .. ( ultérieurement nchalah ..)
	
	5. For Loop : 
	# Lacune fi : Array , Commandes bl option , jabdén bl ligne 
		* Plusieurs Formes Possibles : 
			Forme Générique: 
				for X in Y  
					do 
						Inst1 
						Inst2 
						.     # utiliser le contenu avec $X
						.
						. 
						Instn 
					done 	
			X & Y ynajmou ykouno barcha hajét : 
				- 1 : Bch nparcouri un ensemble fini de VALEURS (lezm $HAJA .. mch esm_variable toul) : 
					-> X : var , Y :  n'importe quel ensemble de VALEURS .. kima : 1 2 3 4 5 ou sami jihen nihel  (".." optionelle ) ou $var1 $var2 ..... $varn ou $* ou ..
				- 2 : Bch na3ml boucle for i in range ... 
					-> X : i , Y : {1..10..2} i yparcouri ml 1 ll 10 (inclus) w yétla3 bl 2 pas .. {START..END..INCREMENT} ( Vérsion bash 4.0 + .. tal9aha fi echo ${BASH_VERSION})
					-> ou bien son équivalent : for ((i=0;i<5;i++))
				- 3 :  Bch nlansi Plusieurs commandes dans l'ordre 
					-> X : command (esm variable) Y : "commande_1" "commande_2" ... "commande_n" 
						echo "..... $command....." # bch tchoufha command fi echo 
						$command # bch tlansi el commande 
						# Voir Q1  
				- 4 : Bch néjbd les outputs ta3 command bl wé7éd bl wé7éd 
					-> X: output Y : $(command linux) ... tnajém dans un cas pratique : for server in $(openstack server list )
					RQ : Mm si command bl option 
					# RQ : Voir PT5 fl Questions .. itérations bl options + sns espace .. 
					# ATTENTION: $command toul matémchich .. lézm $(command)
					PB: bl X w Y héthom .. ya9ra output bl klma bl klma .. kiféh nwali na9rah bl star bl star 
					Code : 
						#!/bin/bash
						ls -l | while IFS= read -r line
						do
							echo "$line"
						done
					Explication du code:
						1/ ls -l : Commande tétlansa w tét3adda à travers pipe ll while IFS= read -r line 
						2/ read -r line : ta9ra input w tsobo fi line .. ki bch tsobo fi line bch tchouf chno
						sépérateur 
						3/ IFS= : Haka nkoul ll bash énou séparateur howa fin de la ligne .. ywali read -r 
						ya9ra él input bl ligne bl ligne 
						4/ while : tdawar él code ligne/ligne 
					CC: 
						Command | while IFS= read -r line -> Ta9ralk l'output ta3 él command BL STAR BL STAR 


				- 5 : Bch nitéri 3la ASS/ARRAY : 
					for value in "${array[@]}" 
						do 
							echo "...$value ..." 
						done 
			
			
	6. while loop : 
		while [ Condition ] # [ Condition ] 3liha nafs les contraintes li hkina fihom fl if 
			do 
			...
			...
			done 
			  	
	7. Until loop : 
		until [ condition ] # Hata lin hal condition twali validée ..
		do 
		... # Commandes Exécutés si condition faux .. 3aks while commandes executés si condition vrai 
		... 
		done 
	RQ : Dès que condition wost until validé .. yokhroj ! 

	8. Case : 
		8.1: Wa9th tésta3mlha ? : Ki yébda 3andk baaaaarcha elif m3a if : if .. elif .. elif n fois .. else )
		w test ysir 3la nafs variable .. twali case khir .. 
		8.2: Syntaxe : 
			case expression in 
				pattern 1 ) 
					commands si pattern1 validé # pas de ;; 
					;; # ;; lwéh w wa9téh ? obligatoire dans la fin du block de commandes bch ya3rf énou 
									# les commandes ss ce case wféw  
				pattern 2 ) 
					commands si pattern2 validé
					 ;;  
				..
				..
				* ) # kima else .. kén ma5atft hata condition ylasi li ta7t * 
					commands si aucun pattern n'est validé 
					;;  
			esac 
		RQ : pour un pattern validé ylansi ses commandes w ywa9af 8ad mayzid ytasti lb9ia 
		+ baaaaaaarcha tba3wil .. 
		# expression tnajém tkoun : $variable  ,  $((expression_arith)) , 
		# pattern ynajém ykoun valeur , "klma" (ll fixe) "reg-exp" (flexible akthr) .. 
		hano barbéch fl expressions & patterns possibles , par Exemple kiféh tkolou ken sbh <  5 ken sbh > 9 etc .... 
		-> [0-9] [10-15] etc .. Hano bch nzido ka PT fl cours carrément mch fl Question 


	9. select loop : 
		# Tésta3mlha ki tasna3 script yintéragi m3a user ... 
			select var in val1 val2 val3  # voir Rq 
			do 
				echo "$var selected" 
				# ba3wél bl $var li thb .. tnajém ta3ml case , etc .. 
			done 

			Rq : bch taffichilk lista haka 
			1) val1 
			2) val2 
			3) val3 
			'#? ..  -> tdakhal noumrou fl prompt hétha 
			w ba3d tétlansa echo " .. " -> 
			w t3awéd ta3tik prompt bch tséléctionni
			RQ : ki tséléctionni valeur mch ml moujoud ysajél 3ando vide w khw sns erreur  

	10. break : 
		-> Nésta3mlouha généralement m3a if bch najém nokhroj mn boucle .. ken fama condition vérifié nlansi 
		break bch nokhroj ml boucle li éna fiha .. ya3ni par exemple yébda 3andk des commandes yétlanséw wost
		boucle .. ama théb tkoun capable enk ta9sihom en cas ou une certaine condition est atteinte 
		RQ : kén boucle wost boucle nokhroj kén ml boucle da5lénia KHW .. w nkaml fl barania 
			#!/bin/bash 
			for (( i=0 ; i<5 ; i++ ))
			do
					for ((j=0;j<5;j++))
					do
							if [ $j -gt 2 ]
							then
									break
							else
									echo "le carré $i , $j " 
							fi
					done
			done

	11. continue :
		-> Nesta3mlouha généralement m3a if wost boucle .. nhb par exemple nlasni les opértions li fl boucle pour tt les i li fl for sauf un cas précisé fl if 
		.. Donc héthi tkhalik .. une fois une condition vérifié .. tét3ada w matlansich des commandes .. 
		par contre break .. une fois condition vérifiée ta9si ml boucle w tokhrj mnha 
		#!/bin/bash 
		for (( i=0 ; i<5 ; i++ ))
		do
				if [ $i -eq 1 -o $i -eq 2 ] # Condition d'exclusion .. lansi pr tt les i sauf pour i=1 ou i=2 
				then
						continue # les instructions ta7t continue kol mayétlanséwch mahma houma w yét3ada ll itération li ba3d (mch kima break t5arjk ml boucle complètemnt)
				else
						echo "La boucle numéro $i " 
				fi
		done

	12. Functions : 
	PB: El return fiha PB .. à voir nchalh ..
		12.1 : Syntaxe : 
			Esm_Fonction () { # Asémi les arguments mayét3adéwch ..
			...
			...
			... 
			}
			ou bien 
			function Esm_Fonction () { # nzidou keyword function 
				...
				... 
			}
			# Question : Manajamch nhot des paramètres bl esm fl entete ? (SEMI RESOLU)
			Réponse : Non Référencement ysir kén kima fl 12.2 

		12.2 : Kiféh nréférenci lparamètre wost lfonction ? 
			$1 $2 .... 
			ATTENTION : A NE PAS CONFONDRE : 
				$1 , $2 ... wost code ta3 fonction ( li bin {} ) héka no9sdou bih noumouro le paramètre numéro x li 3aditou ll fonction ki néditha
				par contre $1 , $2 etc .. wost script l bash hékom no9sdou bihom les paramètres li 3adithom ml ligna ta3 terminal ml bara 
		# Voir Q1  
		A APPRENDRE : 
			SC 1 : $1 w 3adi paramétre ml Cli w paramétre m3a fonction w chouf chkoun li yétconsidéra 
				- ta3 fonction 
			SC 2 : $1 w 3adi paramétre ml Cli w paramétre m3a fonction hotou féra8 
				- mara okhra yékhou mta3 el fonction ( mm si ma3adit chy khtha 0 ) .. mm pas 9e3ed ychouf fl parametre 
				ta3 Cli 
		->  CC : $1 , $2 , ... $n wost corps de la fonction hekom yréférenciw ken les paramétres li 3adithom wa9t nedit 
			fonction soit t3adi des paramétres ou pas ki tnédi fonction .. donc une fois thb testa3ml paramétrouét ta3 cli 
			wost fonction ta3ml haka : 
				#!/bin/bash

				# Function accessing script parameters
				my_function() {
					
					echo "First script parameter: $1"
					echo "Second script parameter: $2"
					echo "Third script parameter: $3"
				}

				# Calling the function with script parameters
				my_function "$1" "$2" "$3"
		-> CC Final : 
			- $i (1,2,3...) .. 
				Wost fonction : yréférenci paramétre li m3odih m3a fonction 
				Bara ml fonction : yréférenci paramétre li m3odih ml Cli 

		12.3 : Kiféh Nédiha + n3adi paramètre ll fonction ?  
			fl appel : Esm_Fonction Parm1 Parm2 ....... Parmn 
			( qoui que ce soit la forme de la syntaxe 1 )
		12.4 : Kiféh nretourni variable mn fonction ? : 
			E1 : Zid return $Esm_Variable fi ékhr l fonction ll valeur li thb tretourniha 
			E2 : 	sum 2 10   # 10.3 
					result=$? #Ta7tha toul .. 
					echo "... $result ... "  
			ATTENTION: Méthode valide kén ki nhb nretourni des entiers !!! 
			En effet, fl script bash manjm nreoutrni bl return kén des entiers !! une fois 
		t7awél ta3ml return $string tjik erreur haka:return: "string": numeric argument required
			Q: Comment faire pour retourner une chaine wili ay autre type ? 
			R: 
				echo "$string" fl lékhr mta3 fonction 
				retour=$(function_appel) 
				-> w haka él chaine retournée tétsab fl retour w tjm wa9tha testa3mlha ...

13/ Arrays: 
    Arrays : # Méllkher : Array howa El liste mta3 linux .. Nsajlou fiha des éléments à manipuler !
        
        1/ Kiféh tasna3 Array ? 
            my_array=(item1 item2 item3)
            Example: fruits=("apple" "banana" "cherry")
            RQ: echo $fruits ta3tik kén l'élément lowl .. bch taffichih lkol lézm echo "${fruits[@]}
        
        2/ Acces ll élémenét mte3o : 
            "${array[index]}" syntax. # NB : indéxation tbda ml 0 fl array 
            Exemple : echo "${fruits[0]}"  # Outputs: apple
        
        3/ Longuer de l'array :
            "${#array[@]}": # Question 1: @ ? tt les éléments ?  (Voir ba3d) .. el # hia cardinal
            length=${#fruits[@]}
            echo "Length of the array: $length"  # Outputs: Length of the array: 3
        
        4/ Tzid élément(s) ll Array:
        fruits+=("el1" "el2" ... "eln") # RQ 1 : El += Lézm lésa9 zouz !! # RQ : élément ajouté flkher ta3 array

        5/ Tdawar boucle for 3al Array:
        for fruit in "${fruits[@]}"; do
            echo "Fruit: $fruit"
        done
        
        6/ Tfasakh élément ml Array bl index mte3o:
        unset fruits[1]  # Removes the second element (banana) (Faméch retour xD
        tfasakh , taffichih tal9ah tna7a khw) 
        
        7/ Slicing an Array:
        slice=("${fruits[@]:1:2}")  # Starts from index 1 (inclusive) and includes 2 elements
            
    2/ @ Symbol with Arrays: 
        * "${array[@]}": 
        Tgénéri un itérable 3al les éléments du tableau lkol najm nesta3mlou fi 2 cas: 
            - UseCase 1 : boucle for 
            - USeCase 2 : N3adi plusieurs arguments ll fonction 

        * UseCase 1 : Boucle for: 
        ```bash
            fruits=("apple" "banana" "cherry")
            for fruit in "${fruits[@]}"; do
            echo "Fruit: $fruit"
            done
        ```
        * UseCase 2 : 
        # Tfixi les paramètres li t7eb t3adihom ll fonction fi array 
        colors=("red" "green" "blue" "yellow")
        # Tdéfini fonction titéri 3al paramétrouét li bch t3adihom
        print_colors() {
            echo "Printing colors:"
            for color in "$@"; do # RQ : el $@ héki pour dire dour 3al les params li t3adéw ll fonction élkol
            echo " - $color"
            done
        }
        # Appel ll fonction t3adi bih les paramètres 
        print_colors "${colors[@]}" 
        # Question: nhb njarab $1 .. 		
        R: $1 hia l'élément lowl fl tableau , $2 hia l'élément théni fl tableau etc ...

14/ Associative Arrays : 
		# Melkher : les équivalents des dictionaires fl python 
		RQ: Ass arrays kén fl bash 4.0+ .. bash --version pr vérifier la version de ton bash 
		1. Declari Associative Array:
			declare -A my_assoc_array 
			On a 3 méthodes d'initialisation: 
			# M1 : declare + initialisation : 
			declare -A colors=(
				["red"]="#FF0000"
				["green"]="#00FF00"
				["blue"]="#0000FF"
			)
			# M2: Voir fl question kif tinisialisi bl for 
		2. Tzid/Tmodifi Key-Value Pairs:
			my_assoc_array["key"]="value" -> M3 : bl kaaba bl kaaba 
			# Si la clé existe ? -> ymodifi sa valeur 
		3. Taccédi ll Values à partir ml Keys:
			echo "Red color: ${colors["red"]}"  
		4. For 3al Associative Array (keys,values):
			- Tdour 3al les clés:
				```bash
				for key in "${!colors[@]}"; do
				echo "Key: $key, Value: ${colors[$key]}"
				done
				```
			- Tdour 3al les valeurs:
				```bash
				for value in "${colors[@]}"; do
				echo "Value: $value"
				done
				```
		5. Check if a Key Exists:
			T7eb t4abét un clé existe ou pas fl dictionaire : 
				```bash
				if [ -v colors["red"] ]; then
				echo "The 'red' key exists."
				else
				echo "The 'red' key does not exist."
				fi
				```
		6. Tfasakh le couple Key-Value :
			Tfasakh couple key:value ml dictionaire ass : 
				```bash
				unset colors["red"]
			```