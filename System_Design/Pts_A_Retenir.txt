Cours 1: Fundamentals - Jahr+  
    - SSH Connexion: (Secure SHell)
    - HTTP: 
        - HTTP Connexion 
        - HTTP Methods 
        - HTTP(S) 
    - SSL/TLS: (Secure Sockets Layer)
    - Caching (In General)
    - LoadBalancing
    - Proxy , Forward Proxy (FP) & Reverse-Proxy (RP) Servers 
    - Loadbalancer vs ReverseProxy
    - API Overview
    - API Gateway
    - CDN
Cours 2: Design Examples on AWS - Lecture1 + 
    - Whats_the_time_contraintes 
    - MyClothes.com 
    - MyBlogcom 
    - Wordpress_app 
    - Mobile_App 
    - Microservices
    - Distributed Paid Content 
Cours 3: Templates - Lecture1 +
    - System Design Master Template (Design Gurus)