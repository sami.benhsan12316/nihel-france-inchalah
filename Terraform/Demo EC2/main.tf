provider "aws" {region= "west-ue-3"}

# déclaration de variables ... 
variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "env-prefix" {}
variable "availability_zone" {}
variable "instance_type" {}
variable "public_key_location" {}

resource "aws_vpc" "myapp-vpc-1" { # E1 
    cidr_block =  var.vpc_cidr_block
    tags = { 
        Name : "${var.env-prefix}-vpc"
    }
}

resource "aws_subnet" "myapp-subnet-1" { #E2 
    vpc_id = aws_vpc.vpc-1.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.availability_zone
    tags = { 
        Name : "${var.env-prefix}-subnet"
    }
}

resource "aws_internet_gateway" "myapp-gateway" { #E3 
    vpc_id = aws_vpc.myapp-vpc-1.id # Gateway associé lél vpc hétha 
    tags = { 
        Name : "${var.env-prefix}-gateway"
    }
}

resource "aws_route_table" "myapp-route-table" {  # E4 
    vpc_id = aws_vpc.myapp-vpc-1.id
    route  { # ligne loula (internal route) configuré automatiquement mch ahna nconfiguriwha .. ahna nconfiguriw kén route externe 
        cidr_block = "0.0.0.0/0" # Pour dire les machine externe 
        gateway_id = aws_internet_gateway.myapp-gateway.id # Pour signaler le traffic li dékhl 3al IGW ml bara 
    }
    tags = { 
        Name : "${var.env-prefix}-rte"
    }
}

resource "aws_route_table_association" "rtb-subnet" { # E5 
    subnet_id = aws_subnet.myapp-subnet-1.id
    route_table_id =  aws_route_table.myapp-route-table.id
}



resource "aws_security_group" "myapp-sg"  { #E6 
		name = "........" 
		vpc_id = aws_vpc.myvpc.id 
		ingress {   # ingress : incoming request .. les requests dékhlin .. héthi ll inboud 1 
			from_port = 22   # Najmou n7lou port ou un interval de ports 
			to_port = 22 
			protocol = "tcp" 
			cidr_blocks = ["var.my_ip"]  # Nhotou hna les @ip li najém né9bl leur traffic .. 
                                            # "my_ip" = '......./32' ma3néha mani bch nautorisi l'acces kén ml local mté3i
		}
		ingress {
			from_port = 8080 
			to_port  = 8080 
			protocol = "tcp" 
			cidr_blocks = ["0.0.0.0/0"]    # ay VM ml bara tnajém ta3ml connexion HTTP 3al VM mté3na ( à partir du browser )
		}
		egress {
			from_port = 0  # tt les ports ynajmou yé9blou notre traffic 
			to_port = 0   
			protocol = "-1"	# Najmou nab34ou avec n'importe quel protocol 
			cidr_blocks =  ["0.0.0.0/0"] # Najmou néb34ou pour n'importe quel machine 
			prefix_list_ids = []  # héthi ll vpc endpoints 9alét tafiwha 
		}
	}


data "aws_ami" "latest-ami" {  # oui .. ami ta7t aws .. dc 3anna aws_ami 
	most_recent = true # nhb the latest name ta3 ami 
	owners = ['amazon'] # Voir RQ 2 bch tchouf kiféh tala3 owner 
	filter { # Voir RQ 3 ... No = fil filter 
		name : "name" # name no9sdou bih critère ... w "name" esm lcritère ne pas confondre 
		values : ['reg-exp-de-ami'] # expression régulier thot biha les valeurs possibles
	}
	filter {
		name : "virtualization-type" 
		values : ['hvm']
	}
}

resource "aws_key_pair" "ssh-key" {
	key_name = "key-pair" 
	public_key = file (var.path_location) # Tq path_location fiha path ta3 clé public li généritha fl local
											+ # Voir RQ 9 + 10 + 11 
}

resource "aws_instance" "myapp-server" {
	ami = data.aws_ami.latest-ami.id # Voir RQ 1 + # L'id ta3 l'objet li tkharajhoulk data.aws_ami howa ami li tlawj alih 
	instance_type = var.instance_type  # Voir RQ 4 + Tanséch Tkoun mdéclari instance_type ka variable + 3atiha valeur fl env file

	# ATTENTION : vpc_id mahiyéch attribut .. ça peut confendre .. hano dima thabét bl plan tf .. 
	# Voir Q5 
	subnet_id = aws_subnet.myapp-subnet.id
    availability_zone = var.availability_zone # Voir RQ 6  
	vpc_security_group_ids = [aws_security_group.my-app-security-group.id ] # Voir RQ 5
	

	associate_public_ip_adress = true # Voir RQ 7 
	user_data = <<EOF  # user_data n3adiw fih Script  yétlansa dès que VM tdémarri + Voir RQ
				#!/bin/bash 
				sudo .............
				.............
				docker run -p8080:8080 ngnix 
				EOF # hékom <<EOF w EOF nhotou fi wosthom script .sh 3adi
	key_name = aws_key_pair.ssh-key.key_name 
	tags = {
		Name : "${var.env}-instance"
	}
}


output "ec2_public_ip" {
    value = aws_instance.myapp-instance.public_ip
}